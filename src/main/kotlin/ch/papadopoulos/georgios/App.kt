package ch.papadopoulos.georgios

import ch.papadopoulos.georgios.cli.Cli
import ch.papadopoulos.georgios.taskmanager.DefaultTaskManager
import ch.papadopoulos.georgios.taskmanager.FifoTaskManager
import ch.papadopoulos.georgios.taskmanager.LinuxAdapter
import ch.papadopoulos.georgios.taskmanager.PriorityTaskManager
import picocli.CommandLine
import java.util.concurrent.Callable

@CommandLine.Command(name = "taskManager", mixinStandardHelpOptions = true,
    description = ["Runs the taskManager with default, fifo or priority algorithm"])
class App : Callable<Int> {
    @CommandLine.Option(names = ["-a", "--algorithm"], description = ["default, fifo, priority"])
    var algorithm = "default"

    override fun call(): Int {
        val taskManager = when(algorithm.lowercase()){
            "fifo" -> FifoTaskManager(LinuxAdapter())
            "priority" -> PriorityTaskManager(LinuxAdapter())
            "default" -> DefaultTaskManager(LinuxAdapter())
            else ->  throw RuntimeException("Invalid algorithm")
        }
        Cli(taskManager).main(listOf())
        return 0
    }
}

fun main(args: Array<String>) {
    CommandLine(App()).execute(*args)
}