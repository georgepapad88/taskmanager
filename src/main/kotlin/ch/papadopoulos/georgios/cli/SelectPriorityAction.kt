package ch.papadopoulos.georgios.cli

import ch.papadopoulos.georgios.taskmanager.Priority

enum class SelectPriorityAction(val label: String, val priority: Priority) {
    LOW("low", Priority.LOW),
    MEDIUM("medium", Priority.MEDIUM),
    HIGH("high", Priority.HIGH);

    companion object {
        fun parse(stringValue: String): SelectPriorityAction? {
            return values().find { it.label.equals(stringValue, ignoreCase = true) }
        }
    }
}