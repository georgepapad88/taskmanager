package ch.papadopoulos.georgios.cli

import ch.papadopoulos.georgios.taskmanager.SortDirection

enum class SelectSortDirectionAction(val label: String, val sortDirection: SortDirection) {
    ASC("ascending", SortDirection.ASC),
    DESC("descending", SortDirection.DESC);

    companion object {
        fun parse(stringValue: String): SelectSortDirectionAction? {
            return values().find { it.label.equals(stringValue, ignoreCase = true) }
        }
    }
}