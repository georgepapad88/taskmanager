package ch.papadopoulos.georgios.cli

import ch.papadopoulos.georgios.taskmanager.DefaultTaskManager
import ch.papadopoulos.georgios.taskmanager.ITaskManager
import ch.papadopoulos.georgios.taskmanager.LinuxAdapter
import ch.papadopoulos.georgios.taskmanager.Priority
import ch.papadopoulos.georgios.taskmanager.SortDirection
import ch.papadopoulos.georgios.taskmanager.SortOrder
import ch.papadopoulos.georgios.taskmanager.Task
import com.github.ajalt.clikt.core.CliktCommand
import com.yg.kotlin.inquirer.components.promptConfirm
import com.yg.kotlin.inquirer.components.promptInput
import com.yg.kotlin.inquirer.components.promptList
import com.yg.kotlin.inquirer.core.KInquirer

/**
 * Controller that Ask user for choice prompts from the given options,
 * and repeat functionality again on end.
 */
class Cli(private val taskManager: ITaskManager = DefaultTaskManager(LinuxAdapter())) : CliktCommand() {
    override fun run() {
        while(true) {
            val choice: Action? = KInquirer.promptList(
                "Please select one of the following actions", Action.values().map { it.label }
            ).let { Action.parse(it) }

            echo(choice)
            when (choice) {
                Action.ADD -> handleAdd()
                Action.LIST -> handleList()
                Action.KILL -> handleKill()
                Action.KILL_GROUP -> handleKillGroup()
                Action.KILL_ALL -> handleKillAll()
                null -> echo("Invalid option")
            }

            val retry: Boolean = KInquirer.promptConfirm("Do you want to try again", default = true)
            if (!retry) {
                return
            }
        }
    }

    private fun handleAdd() {
        val pid: Int
        try{
            pid = KInquirer.promptInput(
                "Enter PID"
            ).toInt()
        }catch (e: NumberFormatException){
            echo("Invalid PID")
            return
        }
        val priority: Priority? = KInquirer.promptList(
            "Enter priority", SelectPriorityAction.values().map { it.label }
        ).let { SelectPriorityAction.parse(it)?.priority }

        taskManager.add(Task(pid, priority!!))
    }

    private fun handleList() {
        val sortOrder: SortOrder = KInquirer.promptList(
            "Enter sort order", SelectSortOrderAction.values().map { it.label }
        ).let { SelectSortOrderAction.parse(it)?.sortOrder } ?: SortOrder.CREATION_TIMESTAMP

        val sortDirection: SortDirection = KInquirer.promptList(
            "Enter sort direction", SelectSortDirectionAction.values().map { it.label }
        ).let { SelectSortDirectionAction.parse(it)?.sortDirection } ?: SortDirection.ASC

        echo(taskManager.list(sortOrder, sortDirection))
    }

    private fun handleKill() {
        val pid: Int
        try{
            pid = KInquirer.promptInput(
                "Enter PID"
            ).toInt()
        }catch (e: NumberFormatException){
            echo("Invalid PID")
            return
        }
        taskManager.kill(pid)
    }

    private fun handleKillGroup() {
        val priority: Priority? = KInquirer.promptList(
            "Enter priority", SelectPriorityAction.values().map { it.label }
        ).let { SelectPriorityAction.parse(it)?.priority }

        priority?.apply {
            taskManager.killGroup(priority)
            echo("Killed all tasks with priority $priority")
        }
    }

    private fun handleKillAll() {
        taskManager.killAll()
        echo("Killed all tasks")
    }
}
