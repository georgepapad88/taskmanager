package ch.papadopoulos.georgios.cli

import ch.papadopoulos.georgios.taskmanager.SortOrder

enum class SelectSortOrderAction(val label: String, val sortOrder: SortOrder) {
    CREATION_TIMESTAMP("by creation timestamp", SortOrder.CREATION_TIMESTAMP),
    PID("by PID", SortOrder.PID),
    PRIORITY("by priority", SortOrder.PRIORITY);

    companion object {
        fun parse(stringValue: String): SelectSortOrderAction? {
            return values().find { it.label.equals(stringValue, ignoreCase = true) }
        }
    }
}