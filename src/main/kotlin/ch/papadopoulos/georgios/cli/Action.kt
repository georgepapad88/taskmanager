package ch.papadopoulos.georgios.cli

/***
 * Action menu actions
 */
enum class Action(val label: String) {
    ADD(label = "add"),
    LIST(label = "list"),
    KILL(label = "kill"),
    KILL_GROUP(label = "killGroup"),
    KILL_ALL(label = "killAll");

    companion object {
        fun parse(stringValue: String): Action? {
            return values().find { it.label.equals(stringValue, ignoreCase = true) }
        }
    }

}