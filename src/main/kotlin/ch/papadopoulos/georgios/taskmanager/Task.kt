package ch.papadopoulos.georgios.taskmanager

import java.time.LocalDateTime

data class Task (
    val pid: Int,
    val priority: Priority,
    val createTimestamp : LocalDateTime = LocalDateTime.now()
){

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Task

        if (pid != other.pid) return false

        return true
    }

    override fun hashCode(): Int {
        return pid
    }
}