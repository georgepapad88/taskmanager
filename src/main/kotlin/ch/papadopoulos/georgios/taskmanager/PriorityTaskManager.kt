package ch.papadopoulos.georgios.taskmanager

import ch.papadopoulos.georgios.Constants.MAXIMUM_NUMBER_OF_TASKS

class PriorityTaskManager(osAdapter: OSAdapter, maxSize: Int = MAXIMUM_NUMBER_OF_TASKS) : ITaskManager(osAdapter, maxSize, mutableSetOf()) {
    override fun handleOverflow(task: Task) {
        val minPriority = processes.minByOrNull { it.priority.value }?.priority
        val taskToBeRemoved = processes
            .filter { it.priority == minPriority && it.priority < task.priority}
            .minByOrNull { it.createTimestamp }

        taskToBeRemoved?.apply {
            kill(taskToBeRemoved.pid)
            add(task)
        }
    }
}