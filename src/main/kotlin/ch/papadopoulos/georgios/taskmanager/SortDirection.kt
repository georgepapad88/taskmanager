package ch.papadopoulos.georgios.taskmanager

enum class SortDirection() {
    ASC,
    DESC;
}