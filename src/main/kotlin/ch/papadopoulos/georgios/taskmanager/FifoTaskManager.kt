package ch.papadopoulos.georgios.taskmanager

import ch.papadopoulos.georgios.Constants.MAXIMUM_NUMBER_OF_TASKS

class FifoTaskManager(osAdapter: OSAdapter, maxSize: Int = MAXIMUM_NUMBER_OF_TASKS) : ITaskManager(osAdapter, maxSize, mutableSetOf()) {
    override fun handleOverflow(task: Task) {
        val taskToBeRemoved = processes.minByOrNull { it.createTimestamp }
        taskToBeRemoved?.apply {
            kill(taskToBeRemoved.pid)
            add(task)
        }
    }
}