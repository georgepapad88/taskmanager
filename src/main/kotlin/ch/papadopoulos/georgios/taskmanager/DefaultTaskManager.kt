package ch.papadopoulos.georgios.taskmanager

import ch.papadopoulos.georgios.Constants.MAXIMUM_NUMBER_OF_TASKS

class DefaultTaskManager(osAdapter: OSAdapter, maxNumberOfTasks: Int = MAXIMUM_NUMBER_OF_TASKS) : ITaskManager(osAdapter, maxNumberOfTasks, mutableSetOf()) {
    override fun handleOverflow(task: Task) {
        println("Skipping task")
    }
}