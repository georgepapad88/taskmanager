package ch.papadopoulos.georgios.taskmanager

enum class Priority(val value: Int) {
    LOW(0),
    MEDIUM( 1),
    HIGH(2)
}