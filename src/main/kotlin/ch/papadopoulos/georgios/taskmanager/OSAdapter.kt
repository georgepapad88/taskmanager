package ch.papadopoulos.georgios.taskmanager

interface OSAdapter {
    fun kill(pid: Int): Boolean
}