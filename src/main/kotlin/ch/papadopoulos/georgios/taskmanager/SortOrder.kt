package ch.papadopoulos.georgios.taskmanager

enum class SortOrder() {
    CREATION_TIMESTAMP,
    PID,
    PRIORITY;
}