package ch.papadopoulos.georgios.taskmanager

abstract class ITaskManager(val osAdapter: OSAdapter,
                            val maxNumberOfTasks: Int,
                            protected var processes: MutableSet<Task> = mutableSetOf()
) {
    protected abstract fun handleOverflow(task: Task)

    @Synchronized fun add(task: Task) {
        if (processes.size < maxNumberOfTasks) {
            processes.add(task)
            println("Added task with PID ${task.pid}")
        } else {
            println("The task manager is full.")
            handleOverflow(task)
        }
    }

    fun list(sortOrder: SortOrder = SortOrder.CREATION_TIMESTAMP, sortDirection: SortDirection = SortDirection.ASC): List<Task> {
        val createTimestampSelector =  {task: Task -> task.createTimestamp}
        val pidSelector =  {task: Task -> task.pid}
        val prioritySelector = {task: Task -> task.priority}

        val sorted = when(sortOrder){
            SortOrder.CREATION_TIMESTAMP -> processes.sortedBy(createTimestampSelector)
            SortOrder.PID -> processes.sortedBy(pidSelector)
            SortOrder.PRIORITY -> processes.sortedBy(prioritySelector)
        }

        return when(sortDirection){
            SortDirection.ASC -> sorted
            SortDirection.DESC -> sorted.reversed()
        }
    }

    @Synchronized fun kill(pid: Int) {
        val taskToBeDeleted = processes.find { it.pid == pid }
        if(taskToBeDeleted != null) {
            kill(taskToBeDeleted)
        }else{
            println("Task with pid $pid could not be found")
        }
    }

    @Synchronized fun killGroup(priority: Priority) {
        val tasksToBeDeleted = processes.filter { it.priority == priority }
        tasksToBeDeleted.forEach {
            kill(it)
        }
    }

    @Synchronized fun killAll() {
        val tasksToBeDeleted = processes.toList()
        tasksToBeDeleted.forEach {
            kill(it)
        }
    }
    @Synchronized private fun kill(task: Task){
        val successfullyKilled = osAdapter.kill(task.pid)
        if(successfullyKilled) {
            processes.remove(task)
            println("Killed task with pid ${task.pid}")
        }else{
            println("Could not kill task with pid ${task.pid}")
        }
    }
}
