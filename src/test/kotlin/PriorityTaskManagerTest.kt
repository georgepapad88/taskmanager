import ch.papadopoulos.georgios.taskmanager.*
import com.flextrade.kfixture.KFixture
import io.mockk.every
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verify
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.EnumSource

internal class PriorityTaskManagerTest{
    val maxNumberOfTasks = 3
    private val osAdapter = mockk<OSAdapter>()
    private var taskManager = PriorityTaskManager(osAdapter, maxNumberOfTasks)
    val fixture = KFixture()

    @BeforeEach
    fun init(){
        every { osAdapter.kill(any()) } returns true
        taskManager = PriorityTaskManager(osAdapter, maxNumberOfTasks)
    }

    @Nested
    inner class Add{
        @Test
        fun `should add task if the taks manager is not full`(){
            val tasks = fixture.jFixture.collections().createCollection(Task::class.java, maxNumberOfTasks)
            tasks.forEach {
                taskManager.add(it)
            }

            assertEquals(tasks.size, taskManager.list().size)
            assertEquals(tasks, taskManager.list())
            tasks.forEach {
                assert(taskManager.list().contains(it))
            }
        }

        @Test
        fun `should kill the the lowest priority task that is the oldest task and add the new one in its place, if the task manager is full`(){
            val taskManagerSpy = spyk(taskManager)
            val task1 = Task(fixture(), Priority.LOW)
            val task2 = Task(fixture(), Priority.LOW)
            val task3 = Task(fixture(), Priority.MEDIUM)
            val tasks = listOf(task1, task2, task3)
            val task = Task(fixture(), Priority.HIGH)
            tasks.forEach {
                taskManagerSpy.add(it)
            }

            taskManagerSpy.add(task)

            assertEquals(tasks.size, taskManager.list().size)
            assert(taskManager.list().contains(task))
            assert(!taskManager.list().contains(task1))
            verify { taskManagerSpy.kill(task1.pid) }
        }

        @Test
        fun `should not add the new task, if the task manager is full and no task has lower priority than the new one`(){
            val taskManagerSpy = spyk(taskManager)
            val task1 = Task(fixture(), Priority.LOW)
            val task2 = Task(fixture(), Priority.LOW)
            val task3 = Task(fixture(), Priority.MEDIUM)
            val tasks = listOf(task1, task2, task3)
            val task = Task(fixture(), Priority.LOW)
            tasks.forEach {
                taskManagerSpy.add(it)
            }

            taskManagerSpy.add(task)

            assertEquals(tasks.size, taskManager.list().size)
            assertEquals(tasks, taskManager.list())
            assert(!taskManager.list().contains(task))
            verify (exactly = 0) { taskManagerSpy.kill(any()) }
        }
    }

    @Nested
    inner class List{
        @Test
        fun `should return tasks in the right order if sorting by create timestamp in ascending order`(){
            val tasks = fixture.jFixture.collections().createCollection(Task::class.java, maxNumberOfTasks)
            tasks.forEach {
                taskManager.add(it)
            }

            val actual = taskManager.list(SortOrder.CREATION_TIMESTAMP, SortDirection.ASC)

            assertEquals(tasks.sortedBy { it.createTimestamp }, actual)
        }

        @Test
        fun `should return tasks in the right order if sorting by create timestamp in descending order`(){
            val tasks = fixture.jFixture.collections().createCollection(Task::class.java, maxNumberOfTasks)
            tasks.forEach {
                taskManager.add(it)
            }

            val actual = taskManager.list(SortOrder.CREATION_TIMESTAMP, SortDirection.DESC)

            assertEquals(tasks.sortedByDescending { it.createTimestamp }, actual)
        }

        @Test
        fun `should return tasks in the right order if sorting by pid in ascending order`(){
            val tasks = fixture.jFixture.collections().createCollection(Task::class.java, maxNumberOfTasks)
            tasks.forEach {
                taskManager.add(it)
            }

            val actual = taskManager.list(SortOrder.PID, SortDirection.ASC)

            assertEquals(tasks.sortedBy { it.pid }, actual)
        }

        @Test
        fun `should return tasks in the right order if sorting by pid in descending order`(){
            val tasks = fixture.jFixture.collections().createCollection(Task::class.java, maxNumberOfTasks)
            tasks.forEach {
                taskManager.add(it)
            }

            val actual = taskManager.list(SortOrder.PID, SortDirection.DESC)

            assertEquals(tasks.sortedByDescending { it.pid }, actual)
        }

        @Test
        fun `should return tasks in the right order if sorting by priority in ascending order`(){
            val tasks = fixture.jFixture.collections().createCollection(Task::class.java, maxNumberOfTasks)
            tasks.forEach {
                taskManager.add(it)
            }

            val actual = taskManager.list(SortOrder.PRIORITY, SortDirection.ASC)

            assertEquals(tasks.sortedBy { it.priority }, actual)
        }

        @Test
        fun `should return tasks in the right order if sorting by priority in descending order`(){
            val tasks = fixture.jFixture.collections().createCollection(Task::class.java, maxNumberOfTasks)
            tasks.forEach {
                taskManager.add(it)
            }

            val actual = taskManager.list(SortOrder.PRIORITY, SortDirection.DESC)

            assertEquals(tasks.sortedByDescending { it.priority }, actual)
        }
    }

    @Nested
    inner class Kill{
        @Test
        fun `should call OSAdapter to kill the task`() {
            val tasks = fixture.jFixture.collections().createCollection(Task::class.java, maxNumberOfTasks).toList()
            tasks.forEach {
                taskManager.add(it)
            }

            taskManager.kill(tasks[0].pid)

            verify { osAdapter.kill(tasks[0].pid) }
        }

        @Test
        fun `should not call OSAdapter to kill the task if task is not found`() {
            val tasks = fixture.jFixture.collections().createCollection(Task::class.java, maxNumberOfTasks).toList()
            val task = fixture<Task>()
            tasks.forEach {
                taskManager.add(it)
            }

            taskManager.kill(task.pid)

            verify (exactly = 0){ osAdapter.kill(tasks[0].pid) }
        }

        @Test
        fun `should remove task from the list of tasks if successfully killed`() {
            every { osAdapter.kill(any()) } returns true
            val tasks = fixture.jFixture.collections().createCollection(Task::class.java, maxNumberOfTasks).toList()
            tasks.forEach {
                taskManager.add(it)
            }

            taskManager.kill(tasks[0].pid)

            assertEquals(tasks.size-1, taskManager.list().size)
            assert(!taskManager.list().contains(tasks[0]))
        }

        @Test
        fun `should not remove task from the list of tasks if not successfully killed`() {
            every { osAdapter.kill(any()) } returns false
            val tasks = fixture.jFixture.collections().createCollection(Task::class.java, maxNumberOfTasks).toList()
            tasks.forEach {
                taskManager.add(it)
            }

            taskManager.kill(tasks[0].pid)

            assertEquals(tasks.size, taskManager.list().size)
            assert(taskManager.list().contains(tasks[0]))
        }
    }


    @Nested
    inner class KillGroup{
        @ParameterizedTest
        @EnumSource(Priority::class)
        fun `should kill all tasks of selected priority`(priority: Priority) {
            val otherPriority = Priority.values().find { it != priority }!!
            val task1 = Task(1, priority)
            val task2 = Task(2, priority)
            val task3 = Task(3, otherPriority)
            val tasks = listOf(task1, task2, task3)
            tasks.forEach {
                taskManager.add(it)
            }
            taskManager.killGroup(priority)

            verify { osAdapter.kill(task1.pid) }
            verify { osAdapter.kill(task2.pid) }
            assert(!taskManager.list().contains(task1))
            assert(!taskManager.list().contains(task2))
        }

        @ParameterizedTest
        @EnumSource(Priority::class)
        fun `should not kill tasks of other priority`(priority: Priority) {
            val otherPriority = Priority.values().find { it != priority }!!
            val task1 = Task(1, priority)
            val task2 = Task(2, priority)
            val task3 = Task(3, otherPriority)
            val tasks = listOf(task1, task2, task3)
            tasks.forEach {
                taskManager.add(it)
            }
            taskManager.killGroup(priority)

            assertEquals(1, taskManager.list().size)
            assert(taskManager.list().contains(task3))
        }
    }


    @Nested
    inner class KillAll{
        @Test
        fun `should call OSAdapter to kill all tasks`() {
            val tasks = fixture.jFixture.collections().createCollection(Task::class.java, maxNumberOfTasks)
            tasks.forEach {
                taskManager.add(it)
            }
            taskManager.killAll()

            tasks.forEach {
                verify { osAdapter.kill(it.pid) }
            }
        }

        @Test
        fun `should remove all tasks from the task manager`() {
            val tasks = fixture.jFixture.collections().createCollection(Task::class.java, maxNumberOfTasks)
            tasks.forEach {
                taskManager.add(it)
            }
            taskManager.killAll()

            assertEquals(0, taskManager.list().size)
        }
    }
}