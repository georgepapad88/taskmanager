## Run with default task manager
`java -cp TaskManager-1.0-SNAPSHOT.jar ch.papadopoulos.georgios.AppKt -a default`

## Run with fifo task manager
`java -cp TaskManager-1.0-SNAPSHOT.jar ch.papadopoulos.georgios.AppKt -a fifo`

## Run with priority task manager
`java -cp TaskManager-1.0-SNAPSHOT.jar ch.papadopoulos.georgios.AppKt -a priority`
